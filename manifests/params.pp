# params class
#
class gwdgmetrics::params {

  $influxdbhost     = 'metrics.gwdg.de'
  $influxdbport     = '8086'
  $influxdbdbname   = 'influxdb'

}

