# Class: stdlib
#
# This module sets telegraf for use with GWDG metrics server
#
class gwdgmetrics (
  $influxdbhost     = $gwdgmetrics::params::influxdbhost,
  $influxdbport     = $gwdgmetrics::params::influxdbport,
  $influxdbdbname   = $gwdgmetrics::params::influxdbdbname,
  $influxdbdbuser   = undef,
  $influxdbdbpass   = undef,
  $hostname         = $::fqdn,
) inherits gwdgmetrics::params {

  if ($influxdbdbuser != undef) and ($influxdbdbpass != undef) {
    class { '::telegraf':
      hostname => $hostname,
      outputs  => {
        'influxdb' => {
          'urls'     => [ "https://${influxdbhost}:${influxdbport}" ],
          'database' => $influxdbdbname,
          'username' => $influxdbdbuser,
          'password' => $influxdbdbpass,
        },
      },
      inputs   => {
        'cpu'       => {
          'percpu'   => true,
          'totalcpu' => true,
        },
        'disk'      => {
          'ignore_fs' => ['tmpfs', 'devtmpfs'],
        },
        'diskio'    => {},
        'kernel'    => {},
        'mem'       => {},
        'processes' => {},
        'swap'      => {},
        'system'    => {},
        'net'       => {},
        'netstat'   => {},
        'statsd'    => {
          'service_address'          => ':8125',
          'percentiles'              => [90.0],
          'parse_data_dog_tags'      => false,
          'allowed_pending_messages' => 10000,
          'percentile_limit'         => 1000,
          'metric_separator'         => '_',
        },
      },
    }

  }

}

