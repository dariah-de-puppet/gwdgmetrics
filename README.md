# gwdgmetrics

Puppet Module for GWDG's metric server

## stand alone use

Assuming you have `puppet` installed, get the telegraf-module with dependencies:
```
puppet module install datacentred-telegraf
```
The output should give you the module path also, clone this repo there as well.

Then install it by
```
puppet apply --show_diff -e "include gwdgmetrics"
```

